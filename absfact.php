<?php
interface iProduct
{
	public function getName();
}
abstract class abFactory
{
	public static function getFabric($name)
	{
		switch($name)
		{
			case(1): return new Fabric1();
			case(2): return new Fabric2();
		}
	}
	abstract public function getProduct(); 
}
class Fabric1 extends abFactory
{
	public function getProduct()
	{
		return new Prod1();
	}
}
class Fabric2 extends abFactory
{
	public function getProduct()
	{
		return new Prod2();
	}
}
class Prod1 implements iProduct
{
	public function getName()
	{
		return 'class ='.__CLASS__;
	}
}
class Prod2 implements iProduct
{
	public function getName()
	{
		return 'class ='.__CLASS__;
	}
}
$f = abFactory::getFabric(1);
$f2 = abFactory::getFabric(2);
echo 'h='.$f->getProduct()->getName();
echo '<br>';
echo 'h='.$f2->getProduct()->getName();
