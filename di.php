<?php 
class dbConfig
{
	private $name;
	private $pass;
	public function __construct($name,$pass)
	{
		$this->name = $name;
		$this->pass = $pass;
	}
	
	
	public function getName()
	{
		return $this->name;
	}
	public function getPass()
	{
		return $this->pass;
	}
}
class dbConnect
{
	private $configation;
	public function __construct(dbConfig $config)
	{
		$this->configation = $config;
	}
	public function getData()
	{
		return [$this->configation->getName(),$this->configation->getPass()];
	}
	
}
$dbConf = new dbConfig('test',12345);
$conn = new dbConnect($dbConf);
var_dump($conn->getData());