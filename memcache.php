<?php 
$memcache_obj = new Memcache();

/* подключение к memcached серверу */
$memcache_obj->connect('localhost', 11211);

/* Установить значение элемента с ключем 'var_key' использую сжатие "на лету" и
временм жизни 50 секунд. */
$memcache_obj->set('var_key', 'some really big variable', MEMCACHE_COMPRESSED, 50);

echo $memcache_obj->get('var_key');