function getTextAreaSelection(textarea) {
    var start = textarea.selectionStart, end = textarea.selectionEnd;
    return {
        start: start,
        end: end,
        length: end - start,
        text: textarea.value.slice(start, end)
    };
}

function detectPaste(textarea, callback) {
    textarea.onpaste = function() {
        var sel = getTextAreaSelection(textarea);
        var initialLength = textarea.value.length;
        window.setTimeout(function() {
            var val = textarea.value;
            var pastedTextLength = val.length - (initialLength - sel.length);
            var end = sel.start + pastedTextLength;
				var field_name=textarea.name;
            callback({
                start: sel.start,
                end: end,
                length: pastedTextLength,
               text: val.slice(sel.start, end),
					field: field_name
            });
        }, 1);
    };
}

function addHiddenField(form,key,val) {
	if(form.elements.namedItem(key)!=null) {
		form.elements.namedItem(key).value+=";"+val;
		return;
	}
	var input=document.createElement('input');
	input.type='hidden';
	input.name=key;
	input.id=key;
	input.value=val;
	form.appendChild(input);
}

var inputList = document.getElementsByTagName("input");
for(i=0;i<inputList.length;i++){
	detectPaste(inputList[i], function(pasteInfo) {
		if(typeof(document.F)!='undefined') f=document.F;
		else if(typeof(document.F1)!='undefined') f=document.F1;
		else return true;
		addHiddenField(f,'sys_wf_pasted',pasteInfo.field+":"+pasteInfo.text);
	});
}
