function  rec_paste(v){	document.F.paste_log.value+=v;	}
	</script>
			<script>
		var taken_hold=0;
		function term_pop(a,sizes){
			if (!sizes) sizes='width=750, height=600,scrollbars=1';
		   	w=window.open(a,'TERMS',sizes);
		}
		function is_taken(v,fld){
			taken_hold=1;
			var Fe=document.forms['F'].elements;
			var u="?page=helpers/ajax_username_taken&login="+escape(v)+'&fld='+fld;
			if(Fe.email.value.length>2) u+=("&alt="+Fe.email.value);
			else{
				var c='.'+String.fromCharCode(Math.floor(Math.random()*26+97))+String.fromCharCode(Math.floor(Math.random()*26+97));
				u+=("&alt="+c);
			}
			document.getElementById('usr_taken').src=u;
		}

		function replace_if_taken(y,alt){
			var msg=['',"Username taken; we have suggested an alternative.",
						"All ready in use please enter a different one",
				 		"Too short","Invalid - Use letter and digits only"];
			var vis=document.getElementById('display_login');
			if (vis) {
				if (y) {
					if(alt.length && y==1){
						if(alt.length==1) {
							document.F.login.value=alt[0];
						} else {
							msg[y]="Someone already has that username.<div class='alt_logins'>Try another? Available:&nbsp; ";
							for(i=0;i<alt.length;i++){
								if(i) msg[y]+='&nbsp; ';
								msg[y]+="<u onclick=\"document.F.login.value='"+alt[i]+"';visual_is_taken(0)\">"+alt[i]+"</u>";
							}
							msg[y]+="</div>";
						}
					}
					if(alt.length==0) vis.innerHTML="<div id=e2>Username taken</div>";
					else vis.innerHTML="<div id=e2>"+msg[y]+"</div>";
				}
				else vis.innerHTML="(This will appear on your profile; no spaces)";
			}
		}

		function visual_is_taken(y,alt){
			taken_hold=0;
			var msg=['',"Display name/Username is taken","All ready in use please enter a different one",
				 		"Too short","Invalid - Use letter and digits only"];
			var vis=document.getElementById('display_login');
			if (vis) {
				if (y) {
					if(alt && alt.length>0) { // 2014-07-15, output message changed
						msg[y]="Someone already has that username.<div class='alt_logins'>Try another? Available:&nbsp; ";
						for(i=0;i<alt.length;i++){
							if(i) msg[y]+='&nbsp; ';
							msg[y]+="<u onclick=\"document.F.login.value='"+alt[i]+"';visual_is_taken(0)\">"+alt[i]+"</u>";
						}
						msg[y]+="</div>";
					}
					vis.innerHTML="<div id=e>"+msg[y]+"</div>";
				}
				else vis.innerHTML="(This will appear on your profile; no spaces)";
			}
		}
		function is_all_empty(F){
			if(taken_hold) {
				setTimeout(function(){ F.submit(); },500);
				return false;
			}
			if(F.login && F.email && F.pass &&
				(F.login.value.length<4 || F.email.value.length<2 || F.pass.value.length<2)) {
				alert("An error has occurred\n-----------------------\n\nIn order to register, you must complete the form!");
				return false;
			}
			return true;
		}
		$('#display_login').html('','');
    $('#display_pass').html('','');
    $("input[name=email]").attr("placeholder", "");
    $("input[name=login]").attr("placeholder", "");
    $("input[name=pass]").attr("placeholder", "");
	$('.df2013dxv1 .submit, .df2013dxv1 .c_check').bind('touchstart mouseenter', function () {
		$(this).addClass('hover');}).bind('touchend mouseleave', function () { $(this).removeClass('hover'); });