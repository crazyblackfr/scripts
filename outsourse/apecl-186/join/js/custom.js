function  rec_paste(v){	document.F.paste_log.value+=v;	}
var taken_hold=0;
		function term_pop(a,sizes){
			if (!sizes) sizes='width=750, height=600,scrollbars=1';
		   	w=window.open(a,'TERMS',sizes);
		}
		function is_taken(v,fld){
			taken_hold=1;
			var Fe=document.forms['F'].elements;
			var u="?page=helpers/ajax_username_taken&login="+escape(v)+'&fld='+fld;
			if(Fe.email.value.length>2) u+=("&alt="+Fe.email.value);
			else{
				var c='.'+String.fromCharCode(Math.floor(Math.random()*26+97))+String.fromCharCode(Math.floor(Math.random()*26+97));
				u+=("&alt="+c);
			}
			document.getElementById('usr_taken').src=u;
		}

		function replace_if_taken(y,alt){
			var msg=['',"Username taken; we have suggested an alternative.",
						"All ready in use please enter a different one",
				 		"Too short","Invalid - Use letter and digits only"];
			var vis=document.getElementById('display_login');
			if (vis) {
				if (y) {
					if(alt.length && y==1){
						if(alt.length==1) {
							document.F.login.value=alt[0];
						} else {
							msg[y]="Someone already has that username.<div class='alt_logins'>Try another? Available:&nbsp; ";
							for(i=0;i<alt.length;i++){
								if(i) msg[y]+='&nbsp; ';
								msg[y]+="<u onclick=\"document.F.login.value='"+alt[i]+"';visual_is_taken(0)\">"+alt[i]+"</u>";
							}
							msg[y]+="</div>";
						}
					}
					if(alt.length==0) vis.innerHTML="<div id=e2>Username taken</div>";
					else vis.innerHTML="<div id=e2>"+msg[y]+"</div>";
				}
				else vis.innerHTML="(This will appear on your profile; no spaces)";
			}
		}

		function visual_is_taken(y,alt){
			taken_hold=0;
			var msg=['',"Display name/Username is taken","All ready in use please enter a different one",
				 		"Too short","Invalid - Use letter and digits only"];
			var vis=document.getElementById('display_login');
			if (vis) {
				if (y) {
					if(alt && alt.length>0) { // 2014-07-15, output message changed
						msg[y]="Someone already has that username.<div class='alt_logins'>Try another? Available:&nbsp; ";
						for(i=0;i<alt.length;i++){
							if(i) msg[y]+='&nbsp; ';
							msg[y]+="<u onclick=\"document.F.login.value='"+alt[i]+"';visual_is_taken(0)\">"+alt[i]+"</u>";
						}
						msg[y]+="</div>";
					}
					vis.innerHTML="<div id=e>"+msg[y]+"</div>";
				}
				else vis.innerHTML="(This will appear on your profile; no spaces)";
			}
		}
		function is_all_empty(F){
			if(taken_hold) {
				setTimeout(function(){ F.submit(); },500);
				return false;
			}
			if(F.login && F.email && F.pass &&
				(F.login.value.length<4 || F.email.value.length<2 || F.pass.value.length<2)) {
				alert("An error has occurred\n-----------------------\n\nIn order to register, you must complete the form!");
				return false;
			}
			return true;
		}
		  $('#display_login').html('','');
    $('#display_pass').html('','');
    $('#displayName > p > b').text('Username:')
	$("input[name=login]").attr("placeholder", "Username");
    $("input[name=fname]").attr("placeholder", "First Name");
	$("input[name=lname]").attr("placeholder", "Last Name");
	$("input[name=email]").attr("placeholder", "Email");
	$("input[name=zip]").attr("placeholder", "Zip");
    $("input[name=pass]").attr("placeholder", "Password");
	// jquery validation
	$('#sbutton').click(function(){
		var error = false;
		$('input[type=text]').removeClass('err');
		$('input[type=text]').each(function(){
			if($(this).val().length == 0) { $(this).addClass('err'); error = true; }
		});
		if ($('#display_login > #e').length > 0) { $('[name=login]').addClass('err'); error = true; }
		var fname = $('[name=fname]');
		var lname = $('[name=lname]');
		if (fname.val().match(/\d/)) { fname.addClass('err'); error = true; }
		if (lname.val().match(/\d/)) { lname.addClass('err'); error = true; }
		var country = $('[name=country]').val();
		var zip = $('[name=zip]');
		if (country == 840) {
			if(zip.val().match(/[a-zA-z]/)) { zip.addClass('err'); error = true; }
		}
		if (error == false) $('#Fr1').submit();
	});