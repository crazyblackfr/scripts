jQuery(document).ready(function () {

//Preloader active
    jQuery(window).load(function () {
        jQuery(".loaded").fadeOut();
        jQuery(".preloader").delay(1000).fadeOut("slow");
        
        
        cntctsShow01();
        cntctsShow02();
    });

// sidenav navbar nav
    jQuery(".button-collapse").sideNav();


// localScroll js
    //2018-10-17	Chrome/Opera problem		jQuery(".navbar-desktop").localScroll();

// Counter 
    jQuery('.statistic-counter').counterUp({
        delay: 10,
        time: 2000
    });

// Mixitube
    jQuery('#mixcontent').mixItUp({
        animation: {
            animateResizeContainer: false,
            effects: 'fade rotateX(-45deg) translateY(-10%)'
        }
    });

// MagnificPopup
    jQuery('.gallery-img').magnificPopup({
        type: 'image',
        gallery: {
            enabled: true
        },
    });

// Home slider
    jQuery('.slider').slider({full_width: true});

// client slider
    jQuery('.carousel').carousel();

// accordion

    var acc = document.getElementsByClassName("accordion");
    var i;

    for (i = 0; i < acc.length; i++) {
        acc[i].onclick = function () {
            this.classList.toggle("active");
            this.nextElementSibling.classList.toggle("show");
        }
    }
    
// nav menu small menu
    jQuery(document).on("scroll", function () {
        if ($(document).scrollTop() > 120) {
            $("nav").addClass("small");
        } else {
            $("nav").removeClass("small");
        }
    });


});








function cntctsShow01()
{
	var row1='800';
	var row2='-406-';
	var row3='3917<br /><br />';
	//document.write(row1+row2+row3);
	$("#cntcts").html($("#cntcts").html() + '<b>'+row1+row2+row3+'</b>');
}

function cntctsShow02()
{
     
    var e = "contact";
    var eH = "ape";
    var eH2 = "jaw.com";
    //document.write("Email: <a href=" + "mail" + "to:" + e + "@" + eH + eH2+ ">" + e + "@" + eH  + eH2 + "</a>");
    $("#cntcts").html($("#cntcts").html() + "Email: <a href=" + "mail" + "to:" + e + "@" + eH + eH2+ ">" + e + "@" + eH  + eH2 + "</a>");
} 



/*----------------------------------------------------*/
// CONTACT FORM WIDGET
/*----------------------------------------------------*/

    $("#contact-form").find('form').submit(function()
    {
        var form = $(this);
        var formParams = form.serialize();
        $.ajax(
        {
            url: '/site/contact',//''contact.php',
            type: 'POST',
            traditional: true,
            data: formParams,
            success: function(data){
                var response = jQuery.parseJSON(data);
            
                if(response['success']=='Yes')
                {
                	$('#contact-form').find('form').slideUp().height('0');
                    $('#contact-form .result').append('<div class="alert-message success     alert alert-success col-md-8 col-sm-12 col-xs-12"><strong>Your message has been sent. Thank you!</strong></div>');                  	
                }                	
                else
                {
                	var errors='';
                	$.each(response, function( index, value ) {
                		if (value[0]!==undefined)
                		errors += value[0]+"\n";
                	});
                	alert(errors);
                }
                
            }
        })
        return false;
    });        