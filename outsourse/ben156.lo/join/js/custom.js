function  rec_paste(v){	document.F.paste_log.value+=v;	}
var taken_hold=0;
		function term_pop(a,sizes){
			if (!sizes) sizes='width=750, height=600,scrollbars=1';
		   	w=window.open(a,'TERMS',sizes);
		}
		function is_taken(v,fld){
			
		}

		function replace_if_taken(y,alt){
			
		}

		function visual_is_taken(y,alt){
			
		}
		function is_all_empty(F){
			if(taken_hold) {
				setTimeout(function(){ F.submit(); },500);
				return false;
			}
			if(F.login && F.email && F.pass &&
				(F.login.value.length<4 || F.email.value.length<2 || F.pass.value.length<2)) {
				alert("An error has occurred\n-----------------------\n\nIn order to register, you must complete the form!");
				return false;
			}
			return true;
		}
		function set_i_am(obj){
			var opt=obj.options[obj.selectedIndex].value.split(':');
			$('.i_am_hidden').val(opt[0]);
			$('.l4_hidden').val(opt[1]);
			//document['RF'].i_am.value=opt[0];
			//document['RF'].looking_for.value=opt[1];
		}
		function max_no_days(){
			var o='',m=$('.p_bday select[name="mon"]').val(),d=$('.p_bday select[name="day"]').val();
			var lim=[0,31,29,31,30,31,30,31,31,30,31,30,31];
			if(lim[m]<d) d=1;
			for(i=1;i<=lim[m];i++){
				o+="<option value="+i+(d==i?" selected":'')+">"+i+"</option>";
			}
			$('.p_bday select[name="day"]').html(o);
		}
		var step_monitor=1;
	var preload=['bg1.jpg'];

	onload=function(){
		setTimeout(function(){
			for(i=0;i<preload.length;i++) {
				var img=new Image();
				img.src='images/'+preload[i];
			}
		},20);
	}

    function scrollWin() {
        window.scrollTo(0, 100);
    }


    function startTimer(duration, display) {
        var timer = duration, minutes, seconds;
        setInterval(function () {
            minutes = parseInt(timer / 60, 10)
            seconds = parseInt(timer % 60, 10);

            minutes = minutes < 10 ? "0" + minutes : minutes;
            seconds = seconds < 10 ? "0" + seconds : seconds;

            display.text(minutes + ":" + seconds);

            if (--timer < 0) {
                timer = duration;
            }
        }, 1000);
    }

    jQuery(function ($) {
        var fiveMinutes = 60 * 20,
            display = $('#time');
        startTimer(fiveMinutes, display);
    });

	//DOTS ON R1
	function dots() {
		$('span.step2,span.step3,span.step4,span.step5,span.step6').removeClass('active');
		if (step_monitor==2) {$('span.step2').addClass('active');}
		if (step_monitor==3) {$('span.step2,span.step3').addClass('active');}
		if (step_monitor==4) {$('span.step2,span.step3,span.step4').addClass('active');}
		if (step_monitor==5) {$('span.step2,span.step3,span.step4,span.step5').addClass('active');}
		if (step_monitor==6) {$('span.step2,span.step3,span.step4,span.step5,span.step6').addClass('active');}
	}

	function show_next(n){
		$(document).ready(function(){
    		step_monitor+=n;
    		dots();
    		$('#Fr1>p,#displayName,p.p_i_am_p,#display_pass,#as_log,#as_next,#as_back,.item1,.item2,.item3,#regshow').hide();
    		$('p.marital_status,p.body_type,p.race,.pr_Headline_p,.about_p,.p_mobile,.p_country,.addr_p,.r2_bn_buttons,.r2_headline,.mybutton3,#r2_found').hide();
    		if (step_monitor==1) {
        		show='.item1,#as_log';
				$("#as_next").text("Next");
    		}
    		else if (step_monitor==2) {
        		show='.item2,p.iam_p,#as_next';
				$("#as_next").text("Next");
    		}
    		else if (step_monitor==3) {
        		show='.item2,p.p_bday,#as_next,#as_back';
				$("#as_next").text("Next");
    		}
    		else if (step_monitor==4) {
        		show='.item2,#regshow,#as_back';
        		$("#as_next").text("Almost Done");
    		}
    		else if (step_monitor==5) {
        		
    		}
    		else if (step_monitor==6) {
    			
        	}
    		else if (step_monitor==7) {
        		show='.item3';
    			$("#answers .col-xs-12 p, #answers2,div.msg").hide();
    			$("#answers .col-xs-12 p:nth-child(2)").delay("3000").fadeIn();
    			$("#answers .col-xs-12 p:nth-child(3)").delay("4000").fadeIn();
    			$("#answers .col-xs-12 p:nth-child(4)").delay("5000").fadeIn();
    			$("#answers .col-xs-12 p:nth-child(5)").delay("6000").fadeIn(function(){
    				show_next(1);
    			});
        	}
    		else if (step_monitor==8) {
        		
    		}
    		else if (step_monitor==9) {
        		
    		}
    		else if (step_monitor==10) {
        		show='#r2_found,.p_mobile,.p_country,.addr_p,.mybutton3';
    		}
    		$(show).slideDown();
		});
	}

	$(document).ready(function(){
		$('.back_step').click(function(e){
			e.preventDefault();
			if (step_monitor==1) return;
			show_next(-1);
		});
		$('.next_step').click(function(e){
			e.preventDefault();
			var err=0;

			$('select.err, span.err, div.err, p.err, input.err, textarea.err').removeClass('err');

			if(step_monitor==3 && document.F.year.selectedIndex<1) {
				err=true;
				$('select[name=year]').addClass('err');
			} else if(step_monitor==4){
				if(taken_hold) {
					setTimeout(function(){ $('#as_next.next_step').click(); },1000); // fire only one button vs 5
					return false;
				}
				if($('#display_login').html().indexOf('Someone')>-1) return false;
				if ($('#displayName input[name=login]').val().length<4) {
					err=true;
					$('#displayName input[name=login]').addClass('err');
				}
				if ($('input[name=pass]').val().length<6 || $('input[name=pass]').val().indexOf(' ')>-1) {
					err=true;
					$('input[name=pass]').addClass('err');
				}
			} else if(step_monitor==5) {
				if ($('input[name=email]').val().length<6 || $('input[name=email]').val().indexOf('@')<3) {
					err=true;
					$('input[name=email]').addClass('err');
				}
			} else if (step_monitor==8) {
				if ($('input[name=pr_header]').val().length<4) {
					err=true;
					$('input[name=pr_header]').addClass('err');
				}
				if ($('textarea[name=general_info]').val().length<10) {
					err=true;
					$('textarea[name=general_info]').addClass('err');
				}
			} else if (step_monitor==9) {
				if	(document.F.body_type.selectedIndex<1) {
					err=true;
					$('select[name=body_type]').addClass('err');
				}
				if	(document.F.race.selectedIndex<1) {
					err=true;
					$('select[name=race]').addClass('err');
				}
				if	(document.F.marital_status.selectedIndex<1) {
					err=true;
					$('select[name=marital_status]').addClass('err');
				}
			} else if (step_monitor==10) {
				if ($('input[name=city]').val().length<1) {
					err=true;
					$('input[name=city]').addClass('err');
				}
				if ($('input[name=state]').val().length<1) {
					err=true;
					$('input[name=state]').addClass('err');
				}
				if ($('input[name=zip]').val().length<1) {
					err=true;
					$('input[name=zip]').addClass('err');
				}
			}
			if(!err) show_next(1);
		});

	    		$( "p.email_p input.input" ).after( "<small class='smallText'>(Must be a valid email address. You will not be able to login if it is incorrect)</small>" );
	    $("#displayName input").attr("placeholder", "Choose a username");
	    $("#displayName b").text("Username:");
	    $("input[name='city']").attr("placeholder", "City");
	    $("input[name='state']").attr("placeholder", "State/Province");
	    $("input[name='zip']").attr("placeholder", "Zip/Postal Code");
	    $("input[name='mobile']").attr("placeholder", "Ex. 1-888-555-1212");
	    $(".about_p strong + small").replaceWith("<small>What are you looking for in a hookup? (25 character min)</small>");
	    $("input[name=login]").attr("maxlength", "22");
	    $(".p_bday b").replaceWith("<b>Birthdate:</b>");

		show_next(0);

	});
	function term_pop(a,sizes){
		if (!sizes) sizes='width=750, height=601,scrollbars=1';
	   	w=window.open(a,'TERMS',sizes);
	}