function term_pop(a,sizes){
			if (!sizes) sizes='width=750, height=600,scrollbars=1';
		   	w=window.open(a,'TERMS',sizes);
		}
		function is_taken(v){
			var u="?page=username_taken&site=membhookup&login="+escape(v);
			if(document.F.email.value.length>2) u+=("&alt="+document.F.email.value);
			else{
				var c='.'+String.fromCharCode(Math.floor(Math.random()*26+97))+String.fromCharCode(Math.floor(Math.random()*26+97));
				u+=("&alt="+c);
			}
			document.getElementById('usr_taken').src=u;
		}
		function visual_is_taken(y,alt){
			var msg=['',"Display name/Username is taken","All ready in use please enter a different one",
				 		"Too short","Invalid - Use letter and digits only"];
			var vis=document.getElementById('display_login');
			if (vis) {
				if (y) {
					if(alt && alt.length>0) { // 2014-07-15, output message changed
						msg[y]="Someone already has that username.<div class='alt_logins'>Try another? Available:&nbsp; ";
						for(i=0;i<alt.length;i++){
							if(i) msg[y]+='&nbsp; ';
							msg[y]+="<u onclick=\"document.F.login.value='"+alt[i]+"';visual_is_taken(0)\">"+alt[i]+"</u>";
						}
						msg[y]+="</div>";
					}
					vis.innerHTML="<div id=e>"+msg[y]+"</div>";
				}
				else vis.innerHTML="(This will appear on your profile; no spaces)";
			}
		}
		function is_all_empty(F){
			if(F.login && F.email && F.pass &&
				(F.login.value.length<4 || F.email.value.length<2 || F.pass.value.length<2)) {
								alert("An error has occurred\n-----------------------\n\nIn order to register, you must complete the form!");
				return false;
			}
			return true;
		}
$(function(){
   function usc_pop(a){
		sizes='width=750, height=600,scrollbars=1';
		w=window.open(a,'TERMS',sizes);
	}

	var email = 0,
		error = 0;

	if (email == 1 && error == 0) { // submit the form if email is present and no errors
		$('#Fr1, #Fr2').submit()
	}

	if (email == 0 || error == 1) { // show the form if email is missing or has errors
		$('input[type=text], input[type=tel]').addClass('form-control');
		$('.email_p').wrap('<div class="form-group"></div>');
		$('.parent-container').show();
	} else {
		$('.parent-container').hide();
	}
});		