 function FreezeScreen(inmsg) {
      var msg = 'Your Data is Being Processed...';

      if(inmsg)
         msg=inmsg;

      scroll(0,0);
      $('#FreezePane').removeClass('FreezePaneOn').addClass('FreezePaneOn');
      $('#InnerFreezePane').html(msg);
   }
   pop_errors = true;
  			$(document).ready(function(){
						$('#jsCheck').addClass('jsCheckHide');
			$('#formbody').removeClass('jsCheckHide');
			
			$('#xsale_active').click(function(e) {
				if($(this).prop('xsale_state') == 1) {
					$(this).prop('checked',false);
					$(this).prop('xsale_state',0);
				} else {
					$(this).prop('checked',true);
					$(this).prop('xsale_state',1);
				}
			});

			if(1) {
				$('#xsale_active').click();
			}

							emsg = $('.error-text:visible').text();
			
			if(highlight_field != '') {
				validate_highlight($("#"+highlight_field), true);
			}

						

			if(emsg) {
				alert(emsg);
			}
			

			$('.toolTip').click(function(e){
					e.preventDefault();
					// check if the toolinfo is already hidden (prevents wonky double click)
					if ($('#toolInfo').css('display') == 'none') {
						$('#toolInfo').css('display', 'inline'); // show the tooltip
					} else {
						$('#toolInfo').css('display', 'none'); // hide the tooltip
					}
				});


			isSubmitting = false;
			$('.zone_J1 form, .zone_J2 form').submit(function(e){
				if(isSubmitting) {
					e.preventDefault();
					return;
				}

				isSubmitting = true;
				if(validate_form()) {
					if($('html').hasClass('zone_J2')) {
						FreezeScreen('Your Data is Being Processed...');
					}
				} else {
					e.preventDefault();
					isSubmitting = false;
					var etop = $(".error-message").offset();
					if(typeof etop !== 'undefined') {
						scroll(0,etop.top);
					}
				}
			});


		});
		