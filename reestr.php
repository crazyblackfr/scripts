<?php 
class Some
{
	protected static $data=[];
	public static function set($name,$value)
	{
		self::$data[$name] = $value;
	}
	public static function get($name)
	{
		return (isset(self::$data[$name]))?self::$data[$name]:'no';
	}
}
$obj = Some::set('Alex','web dev');
print_r(Some::get('Alex'));