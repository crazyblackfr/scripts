<?
class class1
{
	public static function test()
	{
		echo 'cl_1';
	}
	public static function getTest()
	{
		return static::test();
	}
}
class class2 extends class1
{
	public static function test()
	{
		echo 'cl_2';
	}
}
$ob1= new class1();
echo $ob1->getTest();
$ob2= new class2();
echo $ob2->getTest();