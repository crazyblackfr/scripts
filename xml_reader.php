<?php
Class StoreXMLReader
{
	
	private $reader;
	private $tag;
	
	// if $ignoreDepth == 1 then will parse just first level, else parse 2th level too
	
	private function parseBlock($name, $ignoreDepth = 1) {
		if ($this->reader->name == $name && $this->reader->nodeType == XMLReader::ELEMENT) {
			$result = array();
			while (!($this->reader->name == $name && $this->reader->nodeType == XMLReader::END_ELEMENT)) {
				//echo $this->reader->name. ' - '.$this->reader->nodeType." - ".$this->reader->depth."\n";
				switch ($this->reader->nodeType) {
					case 1:
						if ($this->reader->depth > 3 && !$ignoreDepth) {
							$result[$nodeName] = (isset($result[$nodeName]) ? $result[$nodeName] : array());
							while (!($this->reader->name == $nodeName && $this->reader->nodeType == XMLReader::END_ELEMENT)) {
								$resultSubBlock = $this->parseBlock($this->reader->name, 1);
								
								if (!empty($resultSubBlock))
									$result[$nodeName][] = $resultSubBlock;
								
								unset($resultSubBlock);
								$this->reader->read();
							}
						}
						$nodeName = $this->reader->name;
						if ($this->reader->hasAttributes) {
							$attributeCount = $this->reader->attributeCount;
							
							for ($i = 0; $i < $attributeCount; $i++) {
								$this->reader->moveToAttributeNo($i);
								$result['attr'][$this->reader->name] = $this->reader->value;
							}
							$this->reader->moveToElement();
						}
						break;
					
					case 3:
					case 4:
						$result[$nodeName] = $this->reader->value;
						$this->reader->read();
						break;
				}
				
				$this->reader->read();
			}
			return $result;
		}
	}

	public function parse($filename) {
		
		$_res = array();
		if (!$filename) return $_res;
		
		$this->reader = new XMLReader();
		$this->reader->open($filename);
		
		// begin read XML
		while ($this->reader->read())
		{
			//echo '$this->reader->name'.$this->reader->name."<br>";
			if ($this->reader->name == 'Orders')
			{
				// while not found end tag read blocks
				while (!($this->reader->name == 'Orders' && $this->reader->nodeType == XMLReader::END_ELEMENT))
				{
					//$store_category = $this->parseBlock('Order');
					$_res[] = $this->parseBlock('Order');
					//echo '<pre>';print_r($_res);echo '</pre>';
					
					$this->reader->read();
				}
			
			$this->reader->read();
			}
			
		} // while
		
		return $_res;
	} // func
}
class MyPDO extends PDO
{

    const PARAM_host='localhost';
    const PARAM_port='3306';
    const PARAM_db_name='somebase';
    const PARAM_user='root';
    const PARAM_db_pass='';

    public function __construct($options=null)
	{
        parent::__construct('mysql:host='.MyPDO::PARAM_host.';port='.MyPDO::PARAM_port.';dbname='.MyPDO::PARAM_db_name,MyPDO::PARAM_user,MyPDO::PARAM_db_pass,$options);
    }


}


$xmlr = new StoreXMLReader();
$masdata = $xmlr->parse('som_xml.xml');

if($masdata)
{
	try
	{
		$db = new MyPDO();
		$db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
	}
	catch (PDOException $e){ echo 'Error '.$e;}
	$data=array();
	foreach($masdata as $k=>$v)
	{
		if($v)
		{
			//echo 'OrderId = '.$v['OrderId']."<br>";
			//echo 'ProductID = '.$v['ProductID']."<br>";
			//echo 'Quantity = '.$v['Quantity']."<br>";
			//echo 'Price = '.$v['Price']."<br>";
			if($v['OrderId'] && $v['ProductID'] && $v['Quantity'] && $v['Price'])
			{
				$data[] = array((int) $v['OrderId'], (int) $v['ProductID'], (int) $v['Quantity'], str_replace(',','.',$v['Price']));
			}
			
		}
	}
	
	//echo '<pre>';print_r($data);echo '</pre>';
	if($data)
	{
		$stmt = $db->prepare("INSERT INTO xmlorder (orderId, productId, qty,sum) VALUES (?,?,?,?)");
		try {
			$db->beginTransaction();
			foreach ($data as $row)
			{
				$stmt->execute($row);
			}
			$db->commit();
		}catch (Exception $e){
			$db->rollback();
			throw $e;
		}
		echo 'Job done '. date('Ymd H:i:s');
	}
}
echo ' finish'; 
//var_dump($r);